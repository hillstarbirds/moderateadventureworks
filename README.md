# README #

This is a getting started repository to create a asp.net core web site that will read data from an azure database and modify data within the database. 

### Goal

1. Clone the repository with sourcetree to have a local copy of the excersize code
2. Change the code to read and write data into the database. follwing the [exercises](./exercises.md)
3. Commit and push the changes into the bitbucket repository so it can be reviewed.


### How do I get set up? ###
* Tools
  * Visual studio 2015 Community Edition Update 3 minimum [download](https://www.visualstudio.com/vs/community/)
  * Sql server Management Studio [download](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms)
* Dependencies
  * Asp.net Core (asp.net core)
  * Azure Database 
    * server: hillstartest.database.windows.net
    * database: AdventureWorksLT


#### Configuration

Get the source code from [bitbucket.com](https://bitbucket.org/hillstarbirds/moderateadventureworks)

```
git clone https://bitbucket.org/hillstarbirds/moderateadventureworks.git
```

Database configuration: 

When configuring the database, you can use this **connection string**. You can use the username and password provided for yourself.

```
Server=tcp:hillstartest.database.windows.net,1433;Initial Catalog=AdventureWorksLT;Persist Security Info=False;User ID={your_username};Password={your_password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;
```

Using SSMS with the azure database:

You can use SSMS to connect directly to the azure database using the SQL Server Authentication options to connect to the database. 
You can view content and details of the the tables within the AdventureWorksLT database from there.


### Exersizes

* Show Sales of Mountain bikes from the database
* Add a new Mountain bike to the database.

More details [here](./exercises.md)

### Usefull Links? ###

- [Documentation](https://docs.microsoft.com/en-us/aspnet/core/) of ASP.NET Core 
- Using ADO.NET for connection to databases [MSDN](https://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqlcommand(v=vs.110).aspx)
- [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
- Git commands 
  - [Sourcetree](https://www.sourcetreeapp.com/?utm_source=internal&utm_medium=link&utm_campaign=clone_repo_win) UI for GIT repositories.
  - [Git tutorial](https://www.atlassian.com/git/)


### Who do I talk to? ###

Any questions don't hesitate to mail me. 
* Jeroen@hillstar.nl