
# Exercizes for combining Azure Databases and C#.

The easiest implementation for these excersizes would be a console application that connects with the Azure database. 
If you want you can implment a solution where a ASP.NET website is implemented is also accepted. 

A sample project that you can change is added to this repository.
* ConsoleApp for a console project
* ModerateAdventureWorks for a ASP.Net C# Project.


## Exercise 1: Read Data from an Azure Database

Step 1:  Make a connection with the azure databse with the following connectionstring:

```
Server=tcp:hillstartest.database.windows.net,1433;Initial Catalog=AdventureWorksLT;Persist Security Info=False;User ID={your_username};Password={your_password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;
```

Read data using the following SQL statement and show the result to the user of the applicaiton. 

```SQL 
SELECT sod.SalesOrderID, soh.OrderDate, soh.ShipDate, sod.ProductID, sod.LineTotal FROM SalesLT.SalesOrderHeader AS soh
JOIN SalesLT.SalesOrderDetail AS sod ON sod.SalesOrderID = soh.SalesOrderID
JOIN SalesLT.Product p ON p.ProductID = sod.ProductID
JOIN SalesLT.ProductCategory AS pc ON pc.ProductCategoryID = p.ProductCategoryID
WHERE pc.name = 'Mountain Bikes'
```


## Exercise 2: Insert a new Montain Bike Product

Write an SQL insert statement that can insert a new Product with the following values:
* Product Category: Mountain Bike 
* Product Name: Gazelle 15
* Standard Cost: $650
* List Price: $1500
* Color: Purple
* SellStartDate: 01-March-2017

for example:

```SQL 
INSERT INTO SalesLT.Product
  VALUES(name,...)
  ('Gazelle 15'),...
```

Execute the Insert statement on the Azure database. 


List all Mountain Bikes in the database.

